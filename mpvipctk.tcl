#!/bin/sh
# the next line restarts using wish \
    exec wish "$0" -- "$@"

# control mpv using --input-ipc-server option with tcl/tk

package require Tk
package require sqlite3

# print only if DEBUG environment variable is set
proc debugPuts {msg} {
	if {[info exists ::env(DEBUG)] && $::env(DEBUG)} {
		puts stderr $msg
	}
}

# set env(DEBUG) 1

# replace last sequential X with random char like mktemp(1)
proc mktempName {template} {
	set chars {ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789}
	set len [string length $chars]
	set lastx [string last X $template]
	for {set i $lastx} {$i >= 0} {incr i -1} {
		set oldch [string index $template $i]
		if {$oldch != "X"} {
			break
		}
		set newch [string index $chars [expr int(rand() * $len)]]
		set template [string replace $template $i $i $newch]
	}
	return $template
}

# return unused filename
proc mkUnusedFilename {template} {
	set numTries 10
	for {set i 0} {$i < $numTries} {incr i} {
		set newname [mktempName $template]
		if {![file exists $newname]} {
			return $newname
		}
	}
	error {failed to find unused filename}
}

# constant
array set requestId {
	filename 103
	videoParams 104
	playlist 105
	playlistSort 106
	playlistShuffle 107
	playlistChanged 108
}

namespace eval ::mpvipctk {
	namespace eval v {
		# mpv program name
		variable programName "mpv"
		# delay between try to open socket
		variable delayOpenSocketTime 10
		# socket filename
		variable ipcFile [mkUnusedFilename /tmp/mpvsocket.XXXXXX]
		# store channel id for socket connection
		variable ipcChan {}
		# default window width
		variable defwidth 960
		# default window height
		variable defheight 480
		# when file is loaded, store video parameters
		variable videoParams {}
		# rotation value, use this value * 90 degree
		variable rotate 0
		# listbox variable of playlist
		variable playlistVar {}
		# current postion of of playlist
		variable currentPos -1
		# widget mpv video output embed in
		variable frame {}
	}
}

# toggle fullscreen state of toplevel window
proc toggleFullscreen {w} {
	# fullscreen
	if {[wm attributes $w -fullscreen]} {
		debugPuts "fullscreen off"
		wm attributes $w -fullscreen 0
	} else {
		debugPuts "fullscreen on"
		wm attributes $w -fullscreen 1
	}
	# # simulate fullscreen
	# if {[wm attributes $w -zoomed]} {
	# 	wm attributes $w -zoomed 0
	# } else {
	# 	wm attributes $w -zoomed 1
	# }
}

# resize widget (probably frame) and reset its toplevel geometry
proc resizeFrame {w width height} {
	# reset window geometry setting this makes $w configure -width and -height take effect
	wm geometry [winfo toplevel $w] {}
	# necessary ?
	update
	# resize
	$w configure -width $width -height $height
}

# watch eof (program exit) on mpv output
proc mpvipctk::watch_mpv {chan} {
	debugPuts "watch_mpv : $chan"
	puts -nonewline [read $chan]
	if {[eof $chan]} {
		debugPuts "eof $chan"
		close $chan
		mpvipctk::cleanup
	}
}

# invoke mpv program
proc mpvipctk::runMpv {mpvopt} {
	debugPuts "runMpv : $mpvopt"
	set opt {}
	lappend opt --input-vo-keyboard=no
	lappend opt --wid=[winfo id $v::frame]
	lappend opt --input-ipc-server=$v::ipcFile
	lappend opt --player-operation-mode=pseudo-gui
	lappend opt --load-osd-console=no
	lappend opt {*}$mpvopt

	set fid [open [list |$v::programName {*}$opt] r]
	fconfigure $fid -blocking 0
	# watch eof (program exit)
	fileevent $fid readable [list mpvipctk::watch_mpv $fid]
}

# resize window size to video size * num
proc mpvipctk::resizeFactor {num} {
	set size [mpvipctk::getVideoSize]
	if {$size != ""} {
		set w [expr [lindex $size 0] * $num]
		set h [expr [lindex $size 1] * $num]
		resizeFrame $v::frame $w $h
	}
}

# event handler $line is data from socket
proc mpvipctk::handleMpvEvent {line} {
	debugPuts "handleMpvEvent"

	set dataValue [::sq3 eval { SELECT json_extract($line, '$.data')}]
	set eventValue [::sq3 eval { SELECT json_extract($line, '$.event')}]
	set reqValue [::sq3 eval { SELECT json_extract($line, '$.request_id')}]
	set error [::sq3 eval { SELECT json_extract($line, '$.error')}]

	# handle event
	if {$eventValue == "file-loaded"} {
		queryVideoInfo
	}

	# handle user request
	# ignore request if not "error":"success"
	if {$error != "success"} {
		return
	}
	if {$reqValue == $::requestId(filename)} {
		set filename $dataValue
		set basename $filename
		wm title . $basename
	} elseif {$reqValue == $::requestId(videoParams)} {
		# flattening videoparams {{...}} => {...}
		set v::videoParams [join $dataValue]
		# window size is not set ?
		if {[$v::frame cget -width] == 0} {
			mpvipctk::resizeFactor 1
		}
	} elseif {$reqValue == $::requestId(playlist)} {
		# make playlist window
		makePlaylistWindow $line
	} elseif {$reqValue == $::requestId(playlistSort)} {
		# sort playlist
		playlistSort $line
	} elseif {$reqValue == $::requestId(playlistShuffle)} {
		# when this request arrived, playlist is already shuffled
		# send a message playlist changed
		set msg [format {{ "command": ["get_property", "playlist"], "request_id": %s }} $::requestId(playlistChanged)]
		putsSocket $msg
	} elseif {$reqValue == $::requestId(playlistChanged)} {
		# update v::playlistVar using playlist json data
		updatePlaylistVar $line
	}
}

# read socket and handle its message
# chan : channel id to read
proc mpvipctk::readSocket {chan} {
	debugPuts "readSocket : $chan"

	# read one line for parse as json has same key
	while {[gets $chan line] >= 0} {
		# line may contain very big data
		set line_size [string length $line]
		debugPuts "readSocket : gets $line_size length of data"
		if {$line_size < 40000} {
			debugPuts $line
		}

		handleMpvEvent $line
	}
	if {[eof $chan]} {
		debugPuts "eof $chan"
		close $chan
		# exit program
		mpvipctk::cleanup
	}
}

# remove socket file and exit
proc mpvipctk::cleanup {} {
	debugPuts "cleanup"
	file delete $v::ipcFile
	::sq3 close
	exit
}

# puts msg to socket
proc mpvipctk::putsSocket {msg} {
	debugPuts "putsSocket : $msg"
	if {$v::ipcChan == ""} {
		debugPuts "ipcChan is not set"
		return
	}
	if {[catch {puts $v::ipcChan $msg} err]} {
		debugPuts "putsSocket error : $err"
		close $v::ipcChan
		return False
	}
	return True
}

# send keypress event to mpv
proc mpvipctk::sendKey {state keysym unichar} {
	debugPuts "sendKey : $state $keysym $unichar"
	set key {}
	set mod {}
	if {[string is graph -strict $unichar]} {
		set key $unichar
	} else {
		set key $keysym
	}

	# convert key name to mpv-specific key name
	# check : `mpv --input-keylist'
	# escape double quote (")
	# escape backslash (\)
	switch $keysym {
		"Tab" {set key "TAB"}
		"space" {set key "SPACE"}
		"Return" {set key "ENTER"}
		"BackSpace" {set key "BS"}
		"Delete" {set key "DEL"}
		"Insert" {set key "INS"}
		"Escape" {set key "ESC"}
		"quotedbl" {set key {\"}}
		"backslash" {set key {\\}}
	}

	if {$state & 1} {
		set mod ${mod}Shift+
	}
	if {$state & 4} {
		set mod ${mod}Ctrl+
	}
	if {$state & 8} {
		set mod ${mod}Alt+
	}

	set msg [format {{ "command": ["keypress", "%s%s"]}} $mod $key]
	putsSocket $msg
}


# set video original size
# return empty string if failed
proc mpvipctk::getVideoSize {} {
	set width [::sq3 eval { SELECT json_extract($v::videoParams, '$.w')}]
	if {$width == "{}"} {
		return
	}
	set height [::sq3 eval { SELECT json_extract($v::videoParams, '$.h')}]
	if {$height == "{}"} {
		return
	}
	return [list $width $height]
}

# mpv vf options
# rotate image by 90 degrees * num
proc mpvipctk::rotateImage90 {num} {
	incr v::rotate $num
	putsSocket "vf set rotate=PI/2*$v::rotate"
}
proc mpvipctk::removeVF {} {
	putsSocket {vf set ""}
}
proc mpvipctk::vflipImage {} {
	putsSocket {vf set vflip}
}
proc mpvipctk::hflipImage {} {
	putsSocket {vf set hflip}
}

# open playlist window
proc mpvipctk::openPlaylist {} {
	debugPuts "openPlaylist"

	# return if already opened
	if {[winfo exist .playlist]} {
		#destroy .playlist
		focus .playlist
		return
	}

	set msg [format {{ "command": ["get_property", "playlist"], "request_id": %s }} $::requestId(playlist)]
	putsSocket $msg
}

## set v::playlistVar from json data
proc mpvipctk::updatePlaylistVar {line} {
	set v::playlistVar {}
	set v::currentPos -1
	set numPlaylist 0
	# '$.data' is json array like this [{"filename":"FILENAME1","id":ID1},{"filename":"FILENAME2","id":ID2},...]
	::sq3 eval {SELECT DISTINCT json_each.value FROM json_each(json_extract($line, '$.data')) } {
		# select value of array
		# value is value for current element
		::sq3 eval {
			SELECT
			json_extract($value, '$.filename') AS filename,
			json_extract($value, '$.title') AS title,
			json_extract($value, '$.id') AS id,
			json_extract($value, '$.current') AS current
		} {
			# value of each column is stored in a variable with the same name as the column itself
			lappend v::playlistVar [list $numPlaylist "|" $id "|" $title "|" $filename]
			# find current item
			if {$v::currentPos < 0} {
				if {"$current" != "" && "$current" != "no" && "$current" != "false"} {
					set v::currentPos $numPlaylist
				}
			}
			incr numPlaylist
		}
	}

	# move cursor of listbox to current position in playlist window
	# TODO: define listbox path somewhere as global?
	# listbox path
	set lb .playlist.f.lb
	if {[winfo exist $lb]} {
		if {$v::currentPos >= 0} {
			$lb see $v::currentPos
			$lb activate $v::currentPos
			$lb selection set $v::currentPos
		}
	}
}

# make playlist window
# line is mpv json ipc data line
proc mpvipctk::makePlaylistWindow {line} {
	debugPuts "makePlaylistWindow"

	set top [toplevel .playlist]

	## label
	pack [label $top.l -text "index | id | title | filename " -anchor nw] -expand no -fill x
	set frame [frame $top.f]
	## listbox
	# -listvariable takes global variable name
	set lb [listbox $frame.lb -listvariable ::mpvipctk::v::playlistVar -width 80 -height 25]
	## scrollbar
	set sb [scrollbar $frame.sb -command "$lb yview" -orient vertical]
	$lb configure -yscrollcommand "$sb set"

	## bindings for playlist window
	# focus main window and close playlist window
	bind $top <Control-Key-7> { focus $mpvipctk::v::frame ; destroy .playlist}
	# play selected item
	bind $lb <Return> "mpvipctk::playlistPlayIndex \[%W index active\]"
	# open selected item with new player
	bind $lb <Double-Button-1> "mpvipctk::playlistPlayIndex \[%W index active\]"
	# popup right click menu
	bind $lb <Button-2> "mpvipctk::playlistNewPlayer \[%W index active\]"

	## right click menu
	set m [menu $frame.m -tearoff 0]
	$m add command -label "open with new player" -command "mpvipctk::playlistNewPlayer \[$lb index active\]"
	$m add command -label "copy filename to clipboard" -command "clipboard clear ; clipboard append \[mpvipctk::playlistGetFilename \[$lb index active\] \]"
	$m add command -label "sort" -command {
		set msg [format {{ "command": ["get_property", "playlist"], "request_id": %s }} $::requestId(playlistSort)]
		mpvipctk::putsSocket $msg
	}
	$m add command -label "shuffle" -command {
		set msg [format {{ "command": ["playlist-shuffle"], "request_id": %s }} $::requestId(playlistShuffle)]
		mpvipctk::putsSocket $msg
	}
	bind $lb <Button-3> "tk_popup $m %X %Y "

	## set v::playlistVar to update listbox
	updatePlaylistVar $line

	pack $lb -side right -expand yes -fill both
	pack $sb -side right -fill y
	pack $frame -expand yes -fill both

	focus $lb
}

# take playlist from json data, sort it by filename
proc mpvipctk::playlistSort {line} {
	debugPuts "mpvipctk::playlistSort"

	# befor sort, set v::playlistVar
	updatePlaylistVar $line

	# store current file information
	set curItem [lindex $v::playlistVar $v::currentPos]

	# sort playlist by filename (path)
	set list_sorted [lsort -index 6 $v::playlistVar]

	## clear playlist and load sorted list
	# clear playlist; note: playlist-clear do not remove current file from playlist
	set msg {{ "command": ["playlist-clear"]}}
	putsSocket $msg
	# add sorted list to playlist
	foreach f $list_sorted {
		set filename [lindex $f 6]
		loadFile $filename append
	}

	## move current file (first file of playlist) to sorted location and remove duplicate one
	# find current file number from sorted list
	set dupNum [lsearch $list_sorted $curItem]
	# dupNum + 1, because now, playlist is current file + $list_sorted
	incr dupNum
	# remove duplicate current file
	set msg [format {{ "command": ["playlist-remove", "%d"]}} $dupNum]
	putsSocket $msg
	# move current file to sorted location
	set msg [format {{ "command": ["playlist-move", "0", "%d"]}} $dupNum]
	putsSocket $msg

	# sort is done, send a message playlist changed
	set msg [format {{ "command": ["get_property", "playlist"], "request_id": %s }} $::requestId(playlistChanged)]
	putsSocket $msg
}

# play playlist number $num
proc mpvipctk::playlistPlayIndex {num} {
	set msg [format {{ "command": ["playlist-play-index", "%d"]}} $num]
	putsSocket $msg
}

# get filename from playlist number
proc mpvipctk::playlistGetFilename {num} {
	set linePL [lindex $v::playlistVar $num]
	set name [lindex $linePL end]
	debugPuts name=$name
	return $name
}

# play playlist number $num with new player
proc mpvipctk::playlistNewPlayer {num} {
	debugPuts "mpvipctk::playlistNewPlayer $num"

	global argv0

	# get filename from playlist number
	set name [playlistGetFilename $num]

	# execute player
	exec $argv0 $name &
}

# open ipc socket, try until socket file is available
# load mediafiles after open socket
proc mpvipctk::openSocket {} {
	debugPuts "openSocket"

	# wait for mpv creates a socket file
	if {![file exists $v::ipcFile]} {
		debugPuts "delay for socket file : $v::delayOpenSocketTime"
		after $v::delayOpenSocketTime mpvipctk::openSocket
		incr v::delayOpenSocketTime $v::delayOpenSocketTime
		return
	}
	# exit if file is exists but not a socket
	if {[file type $v::ipcFile] != "socket"} {
		puts "$v::ipcFile is not a socket"
		exit
	}
	# exit if there is a socket file, but can not read or write
	if {![file readable $v::ipcFile] || ![file writable $v::ipcFile]} {
		puts "$v::ipcFile is not readable or writable"
		exit
	}

	# open socket
	set v::ipcChan [open [list |socat - $v::ipcFile] r+]
	debugPuts "v::ipcChan=$v::ipcChan"
	fconfigure $v::ipcChan -blocking 0 -buffering line

	# register read socket data handler
	fileevent $v::ipcChan readable [list mpvipctk::readSocket $v::ipcChan]

	# we may not receive file-loaded event, so request information now
	queryVideoInfo
}

# load a media file
# optional flags is "replace" or "append" or "append-play"
proc mpvipctk::loadFile {filename {flags replace}} {
	debugPuts "loadFile : $filename"

	set msg [format {{ "command": ["loadfile", "%s", "%s"]}} $filename $flags]
	putsSocket $msg
}

# send $cmd to socket
# currently, it just call putsSocket
proc mpvipctk::sendCommand {cmd} {
	debugPuts "sendCommand : $cmd"

	putsSocket $cmd
}

# mpv console (`) like window
proc mpvipctk::console {} {
	debugPuts "console"

	# already has console window
	if {[winfo exist .console]} {
		focus .console.e
		return
	}

	set top [toplevel .console]
	set gr [label $top.> -text ">"]
	set e [entry $top.e]
	pack $gr -side left
	pack $e -side right -expand yes -fill both
	focus $e

	bind $e <Return> "mpvipctk::sendCommand \[$top.e get\]"
	bind $top <Escape> [list destroy $top]
}

proc mpvipctk::reload {} {
	debugPuts "reload"
	putsSocket {playlist-play-index current}
}

# query mpv about video filename and video parameters
proc mpvipctk::queryVideoInfo {} {
	debugPuts "queryVideoInfo"
	# get filename
	set msg [format {{ "command": ["get_property", "filename"], "request_id": %s }} $::requestId(filename)]
	mpvipctk::putsSocket $msg

	# get original size of the video
	set msg [format {{ "command": ["get_property", "video-params"], "request_id": %s }} $::requestId(videoParams)]
	mpvipctk::putsSocket $msg
}

proc mpvipctk::makeWindow {w} {
	set v::frame [frame $w.f -background black]

	# cleanup when close window
	wm protocol [winfo toplevel $v::frame] WM_DELETE_WINDOW "mpvipctk::cleanup"

	# change mpv window size to default
	# if has argv, change window size after file is loaded
	if {$::argv == ""} {
		$v::frame configure -width $v::defwidth -height $v::defheight
	}

	# send key event to mpv
	bind $v::frame <Key> [list mpvipctk::sendKey %s %K %A]
	# set video original size
	bind $v::frame <Alt-Key-1> {
		mpvipctk::resizeFactor 1
	}
	# set video double size
	bind $v::frame <Alt-Key-2> {
		mpvipctk::resizeFactor 2
	}
	# set video half size
	bind $v::frame <Alt-Key-0> {
		mpvipctk::resizeFactor 0.5
	}
	# fullscreen
	bind $v::frame <Control-f> [list mpvipctk::sendKey %s %K %A]
	bind $v::frame <Alt-f> [list mpvipctk::sendKey %s %K %A]
	bind $v::frame <Key-f> {
		toggleFullscreen [winfo toplevel %W]
	}
	# rotate
	bind $v::frame <Alt-Key-comma> [list mpvipctk::rotateImage90 -1]
	bind $v::frame <Alt-Key-period> [list mpvipctk::rotateImage90 +1]
	# flip
	bind $v::frame <Alt-Key-greater> [list mpvipctk::vflipImage]
	bind $v::frame <Alt-Key-less> [list mpvipctk::hflipImage]
	# reset
	# note : default ctrl+c binding is quit
	bind $v::frame <Control-Key-c> [list mpvipctk::removeVF]
	# playlist
	bind $v::frame <Control-Key-7> [list mpvipctk::openPlaylist]
	# open file
	bind $v::frame <Control-Key-o> {
		mpvipctk::loadFile [tk_getOpenFile]
	}
	# open directory
	bind $v::frame <Control-Key-q> {
		mpvipctk::loadFile [tk_chooseDirectory]
	}
	# simlate console
	bind $v::frame <grave> {
		mpvipctk::console
	}
	# reload file
	bind $v::frame <Control-Key-e> {
		mpvipctk::reload
	}

	pack $v::frame -fill both -expand yes
	focus $v::frame
}

proc mpvipctk::main {} {
	# before run mpv, the window should be visible
	makeWindow .
	runMpv $::argv
	openSocket
}

sqlite3 ::sq3 :memory:
mpvipctk::main

# Local Variables:
# mode: tcl
# End:
